module.exports = {
  siteMetadata : {
    title : `Depressed Coder's Blog`,
  },
  plugins: [
    `gatsby-plugin-emotion`,
    {
        resolve: `gatsby-plugin-typography`,
        options: {
        pathToConfigModule: `src/utils/typography`,
      },
    },
    {
      resolve:'gatsby-source-filesystem',
      options:{
        name: `markdownfiles`,
        path: `${__dirname}/src/markdownfiles` ,
      }
    },
   
    {
      resolve:`gatsby-transformer-remark`,
      options:{
        plugins:[
          `gatsby-remark-prismjs`,
        ],
      }
    },
   
    
  ],
}