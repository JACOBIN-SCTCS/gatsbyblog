import React from "react";
import Layout from "../components/layout"
import {graphql} from "gatsby";
import { css } from "@emotion/core"

export default({data}) => {
    
    const post  = data.markdownRemark;

    return(
      <div>
          <Layout/>

          <div class="container">
            <div class="row">
              <div class="col-xl-2">

              </div>
              <div class="col-xl-8">
                  <div>
                     <h5><center>{post.frontmatter.title}</center></h5>
                     <center><span class="badge badge-warning">{post.frontmatter.tags[0]}    </span>
                     <b><span css={css`color:gray; margin-left:8px;  font-size:14px; `} >{post.frontmatter.date}</span></b>
                     </center>
                    
                      <div css={css`font-family: 'Roboto', sans-serif; text-align: justify`} dangerouslySetInnerHTML={{ __html: post.html }} />
                      <div css={css`padding-top:20px;`}></div>
                  </div>
              </div>
              <div class="col-xl-2"></div>
            </div>
          </div>
         

      </div>
        
        
    );
}

export const query = graphql`
  query($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      html
      frontmatter {
        title,
        date,
        tags,
      }
    }
  }
`