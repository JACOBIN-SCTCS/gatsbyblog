import React from "react"
import Layout from "../components/layout"
import {graphql} from "gatsby"
import { FaGithub,FaTelegramPlane,FaLinkedinIn } from "react-icons/fa";
import me from '../images/me.png';


export default ({data}) => (
  <div>
    <Layout/>
    <div className="container">
      <div className="row">
        <div class="col-xl-4 col-sm-12">
        <img class="img-fluid" src={me} alt="Logo" />
        </div>
        <div class="col-xl-8 col-sm-12">
            <div  class="card">
              <div class="card-body">
                <div class="card-title">
                    Hello ,<br/>
                    &emsp;&emsp;&emsp;&emsp; &emsp; I am Jacob James K,
                     I am currently pursuing Bachelors in Technology (Computer Science)
                     from Kerala Technological University .
                     <br/>
                     <br/>
                     &emsp;&emsp;&emsp;  &emsp;&emsp;Through this blog I wish to 
                     explain topics in the field of computing and latest trends in technologies 
                     in a concise annd lucid way.

                     <br/>
                     <br/>
                     Reach me at : <a href="https://t.me/jacobinsctcs"><FaTelegramPlane/></a>
                     <br/>
                     My Github Profile : <a href="https://github.com/JACOBIN-SCTCS/"><FaGithub/> </a>
                     <br/>
                     Connect with me on :  <a href="https://www.linkedin.com/in/jacob-james-618925156/"><FaLinkedinIn/></a>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
    <br/>
    <br/>
  </div>
   
)

export const query = graphql`
  query{
    site {
      siteMetadata{
        title
      }
    }
  }

`