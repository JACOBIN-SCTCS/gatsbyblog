import 'bootstrap/dist/css/bootstrap.css';
import React from "react"
import { css } from "@emotion/core"
import { Link, graphql } from "gatsby"
import { rhythm } from "../utils/typography"
import Layout from "../components/layout"

export default ({ data }) => {
  return (
    <div>
    <Layout/>
    
      <div className="container">

        <div className="row">

          <div className="col-xl-2"></div>
          <div  className="col-xl-7">
             
        {data.allMarkdownRemark.edges.map(({ node }) => (
          <div css={css`margin-bottom:38px;`} key={node.id}>
            <Link  style={{ textDecoration: 'none' }}
              to={node.fields.slug}
              css={css`
                text-decoration: none;
                color: inherit;
              `}
            >
              <h5
                css={css`
                  margin-bottom: ${rhythm(1 / 4)};
                  color:#ef6c00;
                `}
                style={{ textDecoration: 'none' }}
              >
                {node.frontmatter.title}{" "}
               
              </h5>
              <small
                  css={css`
                    color: #757575;
                    
                  `}
                  style={{ textDecoration: 'none' }}
                >
                  {node.frontmatter.date}
                </small>
                
              <p css={css`margin-top:6px; color:black; &:hover { color:black;}`} style={{ textDecoration: 'none' }}>&emsp; &emsp;&emsp;&emsp;&emsp;&emsp; &emsp; {node.frontmatter.summary}</p>
            </Link>
          </div>
        ))}
        </div>
         <div class="col-xl-3"></div>
        </div>
      </div>
      </div>
   
  )
}

export const query = graphql`
  query {
    allMarkdownRemark(sort: { fields: [frontmatter___date], order: DESC }) {
      totalCount
      edges {
        node {
          id
          frontmatter {
            title
            date(formatString: "DD MMMM, YYYY")
            summary
          }
          fields {
            slug
          }
          excerpt
        }
      }
    }
  }
`