---
title: "Coherent Narrative Story Generation"
date: "2019-09-01"
summary: "This post are the transcripts for my seminar title \"Coherent Narrative Story Generation\"."
tags: ["Natural Language Processing"]
---

---
<style>
.responsive-wrap iframe{ 
    max-width: 100%;
    height:400px;

    }
@import url('https://fonts.googleapis.com/css?family=Roboto&display=swap');
@media(max-width:480px)
{
    .responsive-wrap iframe
    {
            height:250px;
    }
}

</style>
<div class="responsive-wrap">
<!-- this is the embed code provided by Google -->
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQf-3RkEnBwa092vhRdIZuE93Y_3Mt7qUI3xhLzYrtZcANJsgJzFZTzwl_gpnMQGld2Mos6wIvI711b/embed?start=false&loop=false&delayms=3000" frameborder="0" width="100%" height="290" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
</div>

Below are the transcripts for my seminar presentation which has been showed above.
<br></br>
##### Overview 
---
<br>
<p>The topic that i am going to talk about comes under the area of natural language processing. In Natural language processing we deal with raw text . Computers do only what we program it to do It performs greatly on structured data  But making computers deal with human language is a difficult thing . In Natural Language processing we meant to give machines the capability of dealing with the language which we use . Applications are numerous including search engines, chatbots,document summarization etc along with the shortcomings which needs to be resolved . One such application is Story Generation</p>

</br>

##### Introduction
---
<br>

<p>Story Generation  can be stated generally  as text generation. We expect machines to generate text either spoken/written in a natural language like a human. Elaborating , In Story Generation an agent is given a sentence representing a scene in a story and the agent is expected to fill the rest of the story .  Simple for human beings as we have experience participating in story writting and so on  . But there is a need for a logical flow in the sentences generated so that a person could easily connect the generated sentences with the scene given for elaboration and the sentences are logically connected .</p> 
</br>

##### Text Generation Quality Factors
---
![factors](https://i.imgur.com/2MsZCNl.png)
<p>
Suppose we are given the topic "My Leisure Time" .Suppose these are the sentences which i had generated. So when we talk about the quality of sentences we have written ,  Firstly the sentences must have proper <strong>syntax</strong> They must be grammatically well formed , The english grammar we learnt from school. <br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Apart from that the sentences must have proper <strong>semantics</strong> ie they should convey some meaning. <strong>Context Awareness</strong> makes sentences logically connected. <strong>World Knowledge</strong> is a factor that helps readers/listeners understand when much context is not available like if i haven't mentioned about  Ingress being a game you wouldn't have understood about what it is  unless you have have played/heard about Ingress .</p>
</br>

##### Classical Approaches to Text Generation
---

+ Rule Based Approaches<br>
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Generating Hardcoded responses on encountering certain keywords in Input text Can be done by an average programmer.
+ Statistical Approaches <br>
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Based on patterns learned through statistics from a large collection of text (corpus) can capture grammar correctly.  There are a large number of corpus available in packages like <strong>NLTK</strong> (Brown Corpus ,PenTreeBank ) or you could make your own . Statistical methods uses Bayes rule for text generation. In the next slide i will explain the method


##### Example for Statistical Text Generation
---

![bayesequation](https://i.imgur.com/vubfP2v.png)
<p>
Suppose we have the word "the" we   compute  <strong>P(x|"the")</strong> for every word in the vocabulary . The notation can be abbrievated as <strong> probability of word x coming after the </strong>.  We  choose the word x that maximizes this probability.
</p>
<br>

##### Problems with classical Approaches
---
<br>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; The problem with classical  methods is that they do not generalize well and does not give attention to semantics. <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sometimes probabilities can lead to bias in words generated. Suppose we had trained on corpus containing jokes . Generated text could produce hilarious results like  “The Fridge is Laughing”  . Also words having higher probability will get repeated the problem of which can be solved by using better algorithms.<br><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Then came the major game changer in the field of text generation : <strong> Neural Networks.</strong>
</p>
<br>

##### Neural Networks in NLP
---
![rnn](https://www.knime.com/sites/default/files/fig_1_3.png)
<p>
<br>
A Neural Network takes in several inputs multiplies it by a weight and generates an output by using an activation function . The problem with normal neural networks is that it can easily forget observations that it had seen . By the time we reach the 100th training instance it should have easily forgotten about the first instance. <br><br>
That was why recurrent neural networks/RNN’s were introduced. An RNN remembers information in the form of a hidden state representing memory. An RNN takes in input and a previous hidden state and produces a new hidden state to be given to the next RNN Unit and an output.

</p>
<br>

##### Word Embedings 
---

<p>
But we are dealing with words in NLP whereas neural networks deal with numbers. Then came the notion of word vectors/word embeddings . Basically in word embeddings we place a word in an n dimensional space and use the coordinates as a numerical representation of the word. There are three approaches .
<br>
<br><strong>One Hot Vector </strong>

</p>

![onehotvector](https://miro.medium.com/max/674/1*YEJf9BQQh0ma1ECs6x_7yQ.png)

<br>
<p>
Has large amount of dimensionality which can make computation expensive.
<br><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;That was when Word2Vec and Glove were introduced . The most interesting fact about Glove and Word2ve was  that words with     similar meaning / appearing in the same context    are located closely in the n dimensional space.  Other than that it could also capture word analogies like  <strong>king:man</strong>   like  <strong>queen : woman</strong>   (queen = king-man+woman)
</p>

![kingwoman](https://adriancolyer.files.wordpress.com/2016/04/word2vec-king-queen-composition.png)

<p><br></p>

##### Encoder Decoder RNN Structure
---
![encoderdecoder](https://miro.medium.com/max/1200/1*CjaID6XEki7hH8oQ-RGqvA.png)

<p>
 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;This model consists of two RNN Units . One <strong>Encoder</strong> and <strong>Decoder</strong> .<br><br> The encoder unit is responsible for taking words in sentences one at a time  and producing a hidden state  . After reading all words the final hidden state  compresses info of the entire sentence .<br><br>From that hidden state the Decoder RNN is responsible for generating text as  output one word at a time in the form of word embeddings . The Encoder Decoder RNN was normally used for machine translation tasks where the encoder RNN is trained on source language sentence and the Decoder RNN is trained on the target language . The output from one computation in Decoder is used as an input to the next RNN Unit in the Decoder RNN .
 <br>
 <br>
 Sample Text Generated from RNN
</p>

![obama](https://i.imgur.com/2WoAL7Q.png)
<br>
<br>

##### Hierarchial Encoder Decoder RNN
---
<br>
<p>A Normal Encoder Decoder RNN structure can capture context here and there but for longer texts like paragraphs or documents it  is a challenging problem .That was why a Hierarchical Encoder Decoder RNN model was proposed in 2015 by Li along with Dan Jurafsky of stanford university .</p>

![hierarchial](https://i.imgur.com/PrfLDMR.png)

<br>
<p>
 It has a hierarchial collection of RNN Units where each level captures patterns of different levels like word level , sentence level ,paragraph level etc . Combining Hidden states after end of  each sentences gives a hidden representation of the entire pragraph and such a hidden state is decoded in a hierarchical manner using a Hierarchical Decoder Architecture .  New sentences are generated by combining the previous  sentence level hidden state and the hidden state obtained after generating the current sentence. 
 <br>
 <br>
</p>

##### Architectures For  Story Generation
---

<br>

+ Entity Enhanced Seq2Seq Model
+ Skeleton Based Model 

<br>
<br>

##### Entity Enhanced Model
---
<br>
<p>
In the Entity enhanced Seq2Seq Model entities are considered more important and they play a major role in the next sentence generation . An entity can be a charcter or an object in the story.  In Recurrent Neural Networks instead of considering them just as ordinary text in the story ,they are given a vector representation  of their own .This vector representation is updated as entities are used in sentences .
</p>

<br>


###### Architecture

<p>The model uses a Recurrent Neural Network and generates one word at a time . Attention mechanism is used which takes into account the hidden states which were used previously to be used in the generation of the next word . The hidden state is a combination of three contexts </p>

![entitymodel](https://i.imgur.com/4k3iFm2.png)

+ Previous sentence 
+ Entities so far
+ Words generated of current sentence .

These contexts are combined in the hidden state used for generating the next word  in the sentence . 


###### Coprus used 

Toronto Book Coprus

<br>

##### Skeleton Based Model 
---

<br>
<p>
So  the next model that i am going to introduce is the Skeleton Based Model . In a skeleton based model we do not generate a sentence instantly . Instead what we do is we get a basic form of the sentence which captures only the necessary information . Such a basic form of the sentence can be treated  like a skeleton from which our sentences can be  generated .This is actually similar to the way a human generates text . When we are made to prepare a speech  we logically form a structure for our entire speech in our mind and from that we go on elaborating into the real content.
So in this example we can see by using important words we can get a sort of idea about what the next sentence could be.
</p>

![skeleton](https://i.imgur.com/BOSNidm.png)

<br>

###### Architecture

<br>

![trainskeleton](https://i.imgur.com/XMa1HUw.png)

<br>
<p>
This is the architecture of the model when it is used for training we have a Generative Module which generates our sentences. It has an input to skeleton component , which on giving an input sentence extracts only the important information needed .
. Other than that the generative module also has a skeleton to sentence component which uses the skeleton and uses that inorder to generate the next sentence. The  sentence generated is fedback  for generating the next sentence.
<br>
<br>

To supervise the extraction of skeletons a skeleton extraction module is also used . The skeleton extraction module is trained to produce as output only the important elements which are present in the text given as input .The skeleton extraction module is trained on sentence compression dataset .
<br>
<br>
</p>


![testskeleton](https://i.imgur.com/IILw28b.png)

<p>
    The skeleton extraction module is removed after training and the generative module is only used during the testing phase.
    <br>
    <br>
</p>

###### Input to skeleton Component 

<br>
<p>
The input to skeleton component uses a hierarchial encoder decoder rnn where the decoder rnn is responsible for generating the skeleton . The generated skeletons are compared with the skeletons provided by extraction module for improvement .

</p>

<br>

###### Skeleton to Sentence Component 

<p>
<br>
It uses an encoder decoder rnn and the generated sentence is compared with next sentence in the training corpus .
</p>

<br>

The generative module is trained on a visual story telling dataset . 

<br>

###### Skeleton Extraction Module 

<p>
<br>Used only during the training phase . It produces as output the important words from the text which is given to it as input . It is trained on a sentence compression dataset .The connection between the extraction module and the generative module is made through reinforcement learning. Rewards are produced for each phase of training. The reward is low for bad skeletons given as input to generative module and good for a better one . So the two modules promote each other until they converge.<br><br></p>

###### Training Algorithm

<br>

![trainalgorithm](https://i.imgur.com/VE05rQp.png)

<br>
<br>

##### Evaluation Metrics 
---

<br>

+ Automatic Evaluation

    <p>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Done autoatically by using a machine. A good evaluation metric helps when human annotators are not available for checking the quality of text generation . <br><br>  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    One such evaluatuion metric is the <strong>BLEU</strong> score . BLEU stands for Bilingual Evaluation Understudy . A BLEU score of 1 indicates a better quality text .
    <br>
    <br>
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
     BLEU score is calcualted by comparing words generated by the machine against human generated sentence .  We group the words into n-grams in both sentences . and we divide the number of matching bigrams in the machine generated sentence against the total number of bigrams in the generated sentence . 
     <br><br>


    </p>

    ![exampleBLEU](https://i.imgur.com/HdJiwZ8.png)


+ Human Evaluation

    Done by using human annotators with linguistic background . Annotators are made to read the passages generated and rank the passages in a scale of 10  for both fluency and coherence.
    Fluency checks for grammatical accuracy while coherence measure evaluates the degree of coherence in the story .


<br>


##### Conclusion
---

We have gone through the classical methods to some of the recent methods used in text generation . Human evaluation shows that there still needs improvement in the field and newer ideas and approaches needs to be formulated. for achieving much better degree of coherence . 

<br>



##### References 
---
+  Xu, Jingjing & Zhang, Yi & Zeng, Qi & Ren, Xuancheng & Cai, Xiaoyan & Sun, Xu. (2018). A Skeleton-Based Model for Promoting Coherence Among Sentences in Narrative Story Generation. 

+ Elizabeth Clark, Yangfeng Ji, and Noah A. Smith. 2018. Neural text generation in stories using entity representations as context.

+ Jiwei Li, Minh-Thang Luong, and Dan Jurafsky. 2015. A hierarchical neural autoencoder for paragraphs and documents. 

+ Kishore Papineni, Salim Roukos, Todd Ward, and WeiJing Zhu. 2002. Bleu: a method for automatic evaluation of machine translation.

+ https://machinelearningmastery.com/calculate-bleu-score-for-text-python/














