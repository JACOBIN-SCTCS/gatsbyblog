import 'bootstrap/dist/css/bootstrap.css';
import React from "react"
import { css } from "@emotion/core"
import { useStaticQuery, Link, graphql } from "gatsby"
import { rhythm } from "../utils/typography"


export default ({ children }) =>{
    
    const data = useStaticQuery(
        graphql`
        query{
            site {
              siteMetadata{
                title
              }
            }
          }
        `
    )

 /*return (
  <div
  className='container-fluid'
    css={css`
      margin: 0 auto;
      max-width:100%;
      padding: ${rhythm(1)};
      padding-top: ${rhythm(1.0)};
    `}
  >
    <div className='row'>
    <div className='col-xl-6 col-xs-12'>
    <Link to={`/`}  style={{ textDecoration: 'none' }} >
      <h4
        css={css`
          margin-bottom: ${rhythm(2)};
          display: inline-block;
          font-style: normal;
          text-decoration:none;
          color:#0277bd;
          
        `}
      >
        {data.site.siteMetadata.title}
      </h4>
    </Link>
    </div>
    <div className='col-xl-6 col-xs-12 center-block' css={css`display:block;`}>
    <h5>
    <Link
      to={`/about/`}
      css={css`
        float:right;
        color:#0277bd;
       
       
      `}
      style={{ textDecoration: 'none' }}
    >
      About me
    </Link>
    </h5>
    </div>
    </div>
    
  </div>
 )*/

  return (
    <div>
      <div className='container-fluid' css={css`margin-bottom:30px;
              padding: ${rhythm(1)};
      `}>
        <div class="row">
          <div className="col-xl-6 col-xs-12  " css={css`width:100%;
            @media (max-width:420px)
            {
              text-align:center;
            }
          `}>
            <Link to={`/`}  style={{ textDecoration: 'none' }} >
              <h4 css={css`
              display: inline-block;
              font-style: normal;
              text-decoration:none;
              margin-top:1%;
              color:#0277bd;       
              `}>
              {data.site.siteMetadata.title}

              </h4>
            </Link>
          </div>
          <div className="col-xl-6 col-xs-12">
            <div css={css`
                float:right;
                @media(max-width:480px)
                {
                  float:none;
                  width:100%;
                  margin-top:10px;

                }
                `}>

                  <Link to={`/about/`} css={css`
                          float:right;
                          
                          color:#0277bd;
                          @media(max-width:480px)
                          {
                              width:100%;
                              
                          } `
                        }
                        style={{ textDecoration: 'none' }}
                      >
                         <button type="button" class="btn btn-outline-info btn-block" >
                                About Me
                                </button>
                      </Link>
                    

                </div>
         
          </div>
        </div>
      </div>
    </div>
  );

}